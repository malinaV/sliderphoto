import React, {Component} from 'react';
import Slider from "react-slick";
import './App.css';
import Image from './Image/Image.js';


const VK = window.VK;

window.vkAsyncInit = function() {
  VK.init({
    apiId: 7098002
  });
};

setTimeout(function() {
  var el = document.createElement("script");
  el.type = "text/javascript";
  el.src = "https://vk.com/js/api/openapi.js?162";
  el.async = true;
  document.getElementById("vk_api_transport").appendChild(el);
}, 0);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: []
    };
  }

  vkLogins = () => {
    const items = this.state.items.slice();

    VK.Auth.login(
      VK.access.PHOTOS
    );

    VK.Api.call(
      'photos.getAll',
      {
        v: '5.101',
        photo_sizes: 1
      }, (r) => {
        for (let i in r.response.items) {
          items[i] = r.response.items[i];
        }
         
        this.setState({items});
      }
    );
  }

  render() {
    let settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };

    return (
      <div className="App">
        <header className="App-header">

          <p className="vkLogin" onClick={this.vkLogins}>
            Загрузить фото
          </p>

          <Slider {...settings}>

            {
              this.state.items.map((item) => {
                return (
                  <Image src={item.sizes[item.sizes.length - 1].url} alt="" />)
                }
              )
            }

          </Slider>

        </header>
      </div>
    );
  }

}

export default App;
