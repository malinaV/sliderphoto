import React from 'react'

const Image = props => {
    const {
        src,
        alt
    } = props;

    console.log(src);
    
    return (
        <img src={src} alt={alt} />
    );
};

export default Image
